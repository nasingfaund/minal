
# Minal-OS -- An operating system written with Joy.

## Goal

A minimal operating system with a pretty small core written in Assembler,
from there on written in Joy, booting with qemu or VirtualBox on x86_64.
Small and simple enough, that even i can understand what's going on.

Minal-OS is heavily inspired by the fun i had working with
froggeys [Mezzano-OS](https://github.com/froggey/Mezzano).
The main difference is Mezzano is written by a really really skillfull pro, while Minal is not!
Mezzano is >100 kloc, Minal is <10 kloc.


## Joy

Joy is a programming language developed by Manfred von Thun.
Original Joy is concatenative, stackbased and pure functional.

See [Joy-lang.org](http://joy-lang.org).

Or the [Joy mirror of Kevin Albrecht](http://www.kevinalbrecht.com/code/joy-mirror/index.html).

The main difference of the dialect used here to original Joy is that it emphasizes
functional programming, but is not pure.

The simplest and probably best way to describe Joy may be to see it as a crossover of Forth and Lisp.



## What it can already do.

 * Boot on VirtualBox or Qemu with minimal 2 MB, maximal 1024 MB, standard 64 MB.

 * Print to VGA 80x25 screen with 16 colours.

 * Load and run Joy source code.

 * Garbage collection on lists.

 * Interrupt handling for RTC, PIT and Keyboard.

 * Translate keyboard scancodes to ascii characters.

 * Translate keyboard scancodes to symbols like `Shift-Up`, `Ctrl-Meta-Pagedown` etc.

 * Evaluate expressions read at REPL.

 * Write to serial port COM1 / RS-232, with VirtualBox write to logfile at host.

 * Calculations with floating point values via x87 FPU.


## What really needs to be done.

 * A lot of fine tuning about the exact definition of some words.

 * A lot of testing and bug correction.

 * Try on real hardware.


## Out of scope.

 * Multiprocessing

 * Multicore

 * tcp/ip


## Why?

Because it is an awful lot of fun.


## System Requirements

Development happens on Linux on an intel i3-550, mainly with VirtualBox.
Assembling with fasm-1.73.25

 * Bash instead of make.

 * [fasm](https://flatassembler.net/) Flat assembler 1.73 for Linux.

 * ld linker.

 * grub2-mkrescue.

 * VirtualBox or Qemu.


## Minal OS Keyboard Layout

        +----  94 characters on 2.5 rows  ---------------------------------------+
        |                                     [9]                                |
        |            '   ^   ~   @           {   [   ]   }             SHIFT     |
        |             !   ?   &  -$-  %      -(-  )   <   >   *                  |
        |               \   |   /           #   =   ;   :   _             26     |
        |------------------------------------------------------------------------|
        |           [q]  s   u   g   q   v   p   c   d   w            NORMAL     |
        |             a   o   i  -L-  f   b  -T-  e   n   r   +                  |
        |               y   x   z   h   j   k   m   ,   .   -     26+26+6=58     |
        |------------------------------------------------------------------------|
        |                                    7   8   9                  META     |
        |                                    -4-  5   6                          |
        | 94=126-32                             1   2   3   0             10 94  |
        +------------------------------------------------------------------------+
        [9] : toggle char case
        [q] : quotation marks


## License

        ;=============================================================================
        ;   Minal-OS          An Operating System written with Joy.
        ;   GNU GENERAL PUBLIC LICENSE, Version 3.0, http://www.gnu.org/licenses/
        ;   https://codeberg.org/sts-q/minal
        ;   Copyright (C) 2021 sts-q
        ;   https://sts-q.bitbucket.io
        ;   https://sts-q.codeberg.page
        ;=============================================================================
        ;   This program is distributed in the hope that it will be useful,
        ;   but WITHOUT ANY WARRANTY; without even the implied warranty of
        ;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        ;   GNU General Public License for more details.
        ;=============================================================================



## Acknowledgments

osdev.org without which Minal-OS would not have happend.

narke for his OSDevAsm at Github.

froggey for creating his Lisp Machine Mezzano OS.


## Contact

Created at 2021-04-03 by [sts-q](https://sts-q.bitbucket.io).

First commit at 2021-05-24 by sts-q.

