;============================================================================

; :file:   tests.minal                  Tests included in Minal binary.

;============================================================================

`word-tests {} (

;============================================================================
; :chapter:   ATOM TESTS
; :section:             predefined
; :section:             print
; :section:             read
; :section:             stack shuffling words
; :section:             upstack
; :section:             integer


; :section:             floats

:+              0f 3.125 0f 3.875 +                     -> 7
:sqrt           0f 39.0625 sqrt  100 *                  -> 625
:**             0f 2.5 2 **  100 *                      -> 625
:**             0f 6.25 2 1/x **  10 *                  ->  25
:**             81 4 1/x **                             ->   3
:**             3 4 **                                  ->  81

:?rnd           1031 5 **  5 1/x **         1031 =      -> false
:?rnd           1031 5 **  5 1/x **  ?rnd   1031 =      -> true

:*              ( 210 250 300 325 425 450 280 210 380 410 320 225 215 )Statistic.mean-geometric rnd -> 297


; :section:             boolean

:not            false not                               -> true
:not            true  not                               -> false
:not            true  not not                           -> true

:Bool.and       true  true  Bool.and                    -> true
:Bool.and       true  false Bool.and                    -> false
:Bool.and       false true  Bool.and                    -> false
:Bool.and       false false Bool.and                    -> false

:Bool.or        true  true  Bool.or                     -> true
:Bool.or        true  false Bool.or                     -> true
:Bool.or        false true  Bool.or                     -> true
:Bool.or        false false Bool.or                     -> false


; :section:             comparision

:==             25 25 ==                                -> true
:==             (1 2 3)(1 2 3)==                        -> false
:==             (1 2 3) dup ==                          -> true
:==*            `otto 25 ==*                            -> otto false

;;;:0==            `otto 0==                               -> false     error
:0==            1 0==                                   -> false
:0==*           0 0==*                                  -> 0 true

; :section:             cmp

:cmp            10 11 cmp                               -> -1
:cmp            11 11 cmp                               ->  0
:cmp            12 11 cmp                               ->  1

:cmp            (1) `a  cmp                             ->  1           ; list > symbol > int
:cmp            `a   1  cmp                             ->  1
:cmp            (1)  1  cmp                             ->  1
:cmp             1  `a  cmp                             -> -1
:cmp             1  ()  cmp                             -> -1
:cmp            `a  (1) cmp                             -> -1

:cmp            `abcdf   `abcde cmp                     ->  1
:cmp            `abcde   `abcde cmp                     ->  0
:cmp            `abcde   `abcdf cmp                     -> -1

:cmp            `abcde   `abcd  cmp                     ->  1
:cmp            `abcd    `abcde cmp                     -> -1

:cmp            (1 2 4)(1 2 3) cmp                      ->  1
:cmp            (1 2 3)(1 2 3) cmp                      ->  0
:cmp            (1 2 3)(1 2 4) cmp                      -> -1

:cmp            (1 2 3 4)(1 2 3) cmp                    ->  1
:cmp            (1 2 3)(1 2 3 4) cmp                    -> -1

:cmp            (1(2(3(4    ) 5 )6))  (1(2(3(4    ) 5 )6)) cmp   ->  0
:cmp            (1(2(3(4 5  ) 5 )6))  (1(2(3(4 4  ) 5 )6)) cmp   ->  1
:cmp            (1(2(3(4 4  ) 5 )6))  (1(2(3(4 5  ) 5 )6)) cmp   -> -1
:cmp            (1(2(3(4 4  ) 5 )6))  (1(2(3(4 5  ) 5 )6)) cmp   -> -1
:cmp            (1(2(3(4 abc) 5 )6))  (1(2(3(4 abd) 5 )6)) cmp   -> -1

:cmp            true  false cmp                         ->  1
:cmp            `hallo true cmp                         -> -1

:cmp            4 3 cmp                                 ->  1
:cmp            3 3 cmp                                 ->  0
:cmp            3 4 cmp                                 -> -1
:cmp            Pi 1000000000000 1/x +  Pi  cmp         ->  1
:cmp            Pi Pi cmp                               ->  0
:cmp            Pi 1000000000000 1/x -  Pi  cmp         -> -1
:cmp            Pi 3 cmp                                ->  1
:cmp            Pi 4 cmp                                -> -1
:cmp            3 Pi cmp                                -> -1
:cmp            4 Pi cmp                                ->  1



; :section:             type tests
; :section:             list
; :section:             invoke quotation
; :section:             branching
; :section:             Definitions
; :section:             Symbol
; :section:             VM
; :section:             Stacks
; :section:             Diversa
; :section:             outer world modules


:id             1 2 3 4 id                              -> 1 2 3 4
:+              1 2 3 4 +                               -> 1 2 7

:dup            1 dup                                   -> 1 1
:swap           1 2 swap                                -> 2 1

:consup         nilup 1 consup 2 consup down            -> (2 1)
:uzap           1 2 3 nilup consup consup consup   unconsdown unconsdown unconsdown  uzap ->   1 2 3

:sym?           `a sym?                                 -> true
:sym?           `a sym?*                                -> a true

:patt?*         (1 2 3)( int? int? int?) patt?*         -> (1 2 3) true
:patt?*         (1 2 4)( int? int? int?) patt?*         -> (1 2 4) true
:patt?*         (1 2 a)( int? int? int?) patt?*         -> (1 2 a) false
:patt?*         (1 2  )( int? int? int?) patt?*         -> (1 2  ) false

:=              1 1 ==                                  -> true
:=              1 1 =                                   -> true
:=              `a `a =                                 -> true
:=              `a `b =                                 -> false
:=              `a 10 =                                 -> false
:=              10 `b =                                 -> false
:=              1 () =                                  -> false
:=              1 (1 2 3) =                             -> false
:=              (1 2 3) 1 =                             -> false
:=              ()() =                                  -> true
:=              (1 2 3)() =                             -> false
:=              ()(1 2 3) =                             -> false
:=              (1 2 3)(1 2 3) =                        -> true
:=              (1 2 3)(1 2 3 3) =                      -> false
:=              (1 2 3 3)(1 2 3) =                      -> false
:=              (1 (a (bb(cc))))   (1 (a (bb(cc)))) =   -> true
:=              (1 (a (bb(cc)x)))  (1 (a (bb(cc)))) =   -> false
:=              (1 (a (bb(cc))))   (1 (a (bb(cc)x))) =  -> false
:=              (1 (a (bb(cc))))   (1 (a (bb(cd)))) =   -> false

:>              1 2 >                                   -> false
:>              2 2 >                                   -> false
:>              3 2 >                                   -> true
:>              `abc `abc >                             -> false
:>              `abc `acb >                             -> false
:>              `acb `abc >                             -> true
:>              `abc `ab  >                             -> true
:>              `ab  `abc >                             -> false
:>              (1 1)(1 2)>                             -> false
:>              (1 2)(1 2)>                             -> false
:>              (1 3)(1 2)>                             -> true
:>              (1 2 3)(1 2 3)>                         -> false
:>              (1 3 2)(1 2 3)>                         -> true
:>              (1 2)(1 2 3)>                           -> false
:>              (1 2 3)(1 2)>                           -> true

:<              1 2 <                                   -> true
:<              2 2 <                                   -> false
:<              3 2 <                                   -> false
:<              `a `b <                                 -> true
:<              `b `b <                                 -> false
:<              `c `b <                                 -> false
:<              (1 1)(1 2)<                             -> true
:<              (1 2)(1 2)<                             -> false
:<              (1 3)(1 2)<                             -> false

:within?        1 1 3 within?                           -> true
:within?        2 1 3 within?                           -> true
:within?        3 1 3 within?                           -> false
:within?*       1 1 3 within?*                          -> 1 true
:within?*       2 1 3 within?*                          -> 2 true
:within?*       3 1 3 within?*                          -> 3 false


:unswons        (1 2 3) unswons                         -> (2 3) 1
:swonsd         (1 2 3) 4 99 swonsd                     -> (4 1 2 3) 99
:join           (1 2 3) 4 join                          -> (1 2 3 4)
:join           ()      4 join                          -> (4)
:join           (1)     4 join                          -> (1 4)
;;;:rev            ()rev                                   -> ()        ; error, reversing same list over and over
;;;:rev            (1 2 3)rev                              -> (3 2 1)
:copy           () copy                                 -> ()
:copy           (1) copy                                -> (1)
:copy           (1 2) copy                              -> (1 2)
:copy           (1 2 3) copy                            -> (1 2 3)
:copy           (1 2 3 4) copy                          -> (1 2 3 4)
:copy           1000 range0 dup copy  =                 -> true
:copy           1000 range0 dup copy  ==                -> false
:pair           1 2 3 pair 4                            -> 1 (2 3) 4
:1drop          (1) 1drop                               -> ()
:1drop          (1 2 3) 1drop                           -> (2 3)
:unpair         (1 2)unpair                             -> 1 2
:list           25 list                                 -> (25)
;;;:unpair    (1 )unpair                              -> 1 2            ; error
;;;:unpair    (1 2 3)unpair                           -> 1 2            ; error

:concat         ()() concat                             -> ()
:concat         (1 2 3)()concat                         -> (1 2 3)
:concat         ()(4 5 6)concat                         -> (4 5 6)
:concat         (1 2 3 )(4 5 6)concat                   -> (1 2 3 4 5 6)

:unjoin         (1 2 3 4)unjoin                         -> (1 2 3) 4
:cons2          1 11 (2 3) (22 33) cons2                -> (1 2 3) (11 22 33)

:take           ()          3 take                      -> ()
:take           (1 2 3 4) -10 take                      -> ()
:take           (1 2 3 4)   0 take                      -> ()
:take           (1 2 3 4)   1 take                      -> ( 1 )
:take           (1 2 3 4)   2 take                      -> ( 1 2 )
:take           (1 2 3 4)   4 take                      -> ( 1 2 3 4 )
:take           (1 2 3 4)  40 take                      -> ( 1 2 3 4 )

:drop           ()          3 drop                      -> ()
:drop           (1 2 3 4) -10 drop                      -> (1 2 3 4)
:drop           (1 2 3 4)   0 drop                      -> (1 2 3 4)
:drop           (1 2 3 4)   1 drop                      -> (2 3 4)
:drop           (1 2 3 4)   2 drop                      -> (3 4)
:drop           (1 2 3 4)   4 drop                      -> ()
:drop           (1 2 3 4)  40 drop                      -> ()

:infra          ()()infra                               -> ()
:infra          (1 2 3 4)()      infra                  -> (1 2 3 4)
:infra          (1 2 3 4)( swap )infra                  -> (2 1 3 4)
:infra          (1 2 3 4)( +    )infra                  -> (3 3 4)
:infra          (1 2 3 4)( + *  )infra                  -> (9 4)

:filter         ()              ( 1 == )filter          -> ()
:filter         (1 2 3 )        ( 1 == )filter          -> (1)
:filter         (2 3 1)         ( 1 == )filter          -> (1)
:filter         (2 3 4)         ( 1 == )filter          -> ()
:filter         (1 2 1 3 1 4 1) ( 1 == )filter          -> (1 1 1 1)
:filter         (1 1 1 1)       ( 1 == )filter          -> (1 1 1 1)
:filter         ((1 2)(3 4)(5 6)(1 2))            ( (1 2) = )filter         -> ((1 2)(1 2))
:filter         (a b c a b c a b c)               ( `b == )filter           -> (b b b)
:filter         ((a 1)(b 2)(c 3)(d 2)(e 4)(f 2))  ( second 2 == )filter     -> ((b 2)(d 2)(f 2))

:separate       ((a 1)(b 2)(c 3)(d 2)(e 4)(f 2))  ( dup second 2 == )separate   -> ((b 2)(d 2)(f 2)) ((a 1)(c 3)(e 4))

:sort           ( 2 8 4 3 2 )sort                       -> ( 2 2 3 4 8 )
:sort           ( add acc acd adc  abb )sort            -> (abb acc acd adc add)
:sort           ( add acccc acddd adc  acc abb )sort    -> (abb acc acccc acddd adc add)
:sort           ((2 2)(4 5)(8 5)(5 6))sort              -> ((2 2)(4 5)(5 6)(8 5))

:sorton         ((2 2)(4 5)(8 1)(5 6)) (first)sorton    -> ((2 2)(4 5)(5 6)(8 1))
:sorton         ((2 2)(4 5)(8 1)(5 6)) (second)sorton   -> ((8 1)(2 2)(4 5)(5 6))
:sorton         ((2 2)(4 5)(8 1)(5 6)) (sum)sorton      -> ((2 2)(4 5)(8 1)(5 6))

:find           ((1 2 3) (4 5 6) (7 8 9))  (third 5 >)find  -> ((4 5 6)(7 8 9))
:find           (this is a string) (`a ==)find          -> (a string)
:find           "hallo world" ('w' ==)find              -> "world"

:findseq        "this is a string"  "is a"  findseq     -> "is a string"
:findseq        "this is a string"  "is A"  findseq     -> false
:findseq        ""                  "is a"  findseq     -> false
:findseq        "this is a string"  ""      findseq     -> "this is a string"
:findseq        ""                  ""      findseq     -> ""

:subseq?        "this is a string"  "this"  subseq?     -> true
:subseq?        "this is a string"  "is a"  subseq?     -> true
:subseq?        "this is a string"  "ing"   subseq?     -> true
:subseq?        "this is a string"  "inG"   subseq?     -> false
:subseq?        "this is a string"  "h"     subseq?     -> true
:subseq?        "this is a string"  ""      subseq?     -> true

:headseq?        "this is a string"  "this" headseq?    -> true
:headseq?        "this is a string"  "th"   headseq?    -> true
:headseq?        "this is a string"  "t"    headseq?    -> true
:headseq?        "this is a string"  "his"  headseq?    -> false
:headseq?        "this is a string"  ""     headseq?    -> true
:headseq?        "ottomark2"         "ottomark" headseq? -> true
:headseq?        "ottomark"          "ottomark" headseq? -> true
:headseq?        "otto"              "ottomark" headseq? -> true
:headseq?        ""                  "ottomark" headseq? -> true
:headseq?        ""                  ""     headseq?    -> true

:unrepeat        "abbbcccdddde" "bc" unrepeat           -> "abcdddde"
:unrepeat        "hallo   world    off!!!  " " !" unrepeat  -> "hallo world off! "

:cleave         "this is a string"  " a "  cleave       -> "this is"   " a "   "string"   true
:cleave         "this is a string"  " A "  cleave       -> "this is a string"             false

:tailseq?       "this is a string"  "this" tailseq?     -> false
:tailseq?       "this is a string"  "string" tailseq?   -> true

:removeseq      "this is a string"  "is a " removeseq    -> "this string"
:removeseq      "this is a string"  "is A " removeseq    -> "this is a string"

:removeseq      "this is a string"  "is a " "IS MY " replaceseq    -> "this IS MY string"
:removeseq      "this is a string"  "is A " "IS MY " replaceseq    -> "this is a string"

:chunks         "this is a string"                " "  chunks          -> ("this""is""a""string")
:chunks         " this   is    a   string    "    " "  chunks          -> ("this""is""a""string")
:chunks         " this,is    a,,,,string  ,  "    ", " chunks          -> ("this""is""a""string")

:chunks/retain-gaps         ",this,is,a,,,,string,"    "," chunks/retain-gaps lf      -> ("" "this""is""a" "" "" "" "string" "")

:inline-P       ( a b c P x y z) (my cool prog) inline-P  -> (a b c my cool prog x y z)
:inline-P       ( a P c P x P P z)  (q s)       inline-P  -> (a q s c q s x q s q s z)
:replace-P      ( a b c P x y z) (my cool prog) replace-P -> (a b c (my cool prog) x y z)
:replace-P      ( a P c P x P P z)  (q s)       replace-P -> (a (q s) c (q s) x (q s)(q s) z)

:untriple       ( 1 2 3 )untriple                       -> 1 2 3
;;;:untriple       ( 1 2     )untriple                  -> error
;;;:untriple       ( 1 2 3 4 )untriple                  -> error

:member==?      ( 1 2 3 4 )             3 member==?     -> true
:member==?      ( 1 2 3 4 )             7 member==?     -> false
:member==?      ((1 2)(1 3)(1 4))       (1 2) member==? -> false
:member==?      (1 2) dup  ((1 3)(1 4))cons swap  member==?  -> true

:member?      ((1 2)(1 3)(1 4))         (1 2) member?   -> true

;-----------------------------------------------------------------------------
; :chapter:   PRELIB TESTS

:Symbol.keyword?        `let Symbol.keyword?            -> true
:Symbol.keyword?        `funcs Symbol.keyword?          -> true
:Symbol.keyword?        `moo Symbol.keyword?            -> false


;-----------------------------------------------------------------------------
; :chapter:   STDLIB TESTS

:rotate         1 2 3 rotate                            -> 3 2 1
:rollup         1 2 3 rollup                            -> 2 3 1
:rolldown       1 2 3 rolldown                          -> 3 1 2

:2**            4 2**                                   -> 16
:**             4 2 **                                  -> 16
:**             4 1 **                                  ->  4
:**             4 0 **                                  ->  1

:fac             1 fac                                  -> 1
:fac             5 fac                                  -> 120
:fac             7 fac                                  -> 5040
:fac            10 fac                                  -> 3628800
:fac            14 fac                                  -> 87178291200
:fac            19 fac                                  -> 121645100408832000

:fib             5 fib                                  -> 5
:fib             7 fib                                  -> 13
:fib            12 fib                                  -> 144
;;;:fib            42 fib                                   -> 267914296  3

:ack            3 4 ack                                 ->  125
:ack            3 6 ack                                 ->  509
:ack            3 7 ack                                 -> 1021
;;;:ack            3 13 ack                                -> 65533

:shunt          ()(a)shunt                              -> (a)()
:shunt          (1 2 3)(a b c)shunt                     -> (a 1 2 3)(b c)
;;;:shunt          (1 2 3)()shunt                       -> (1)()  ; error

:range0         0 range0                                -> ()
:range0         1 range0                                -> (0)
:range0         3 range0                                -> (0 1 2)
:range1         0 range1                                -> ()
:range1         1 range1                                -> (1)
:range1         3 range1                                -> (1 2 3)

:fold           (1 2 3 4) 0 (+) fold                    -> 10
:fold           (1 2 3 4) 1 (*) fold                    -> 24
:foldl1         (1 2 3 4) (+) foldl1                    -> 10

:enconcat       ((1 2)(3 4)(5 6)) `a   enconcat         -> ( 1 2 a 3 4 a 5 6 )
:inconcat       ((1 2)(3 4)(5 6)) (a b)inconcat         -> ( 1 2 a b 3 4 a b 5 6 )

:remove         () 2 remove                             -> ()
:remove         (2) 2 remove                            -> ()
:remove         (3) 2 remove                            -> (3)
:remove         (1 2 3 1 2 3) 2 remove                  -> (1 3 1 2 3)
:removeall      () 2 removeall                          -> ()
:removeall      (2) 2 removeall                         -> ()
:removeall      (3) 2 removeall                         -> (3)
:removeall      (1 2 3 1 2 3) 2 removeall               -> (1 3 1 3)

:nub            ()nub                                   -> ()
:nub            (1 2 3)nub                              -> (3 2 1)
:nub            (1 2 3 1 2 3 )nub                       -> (3 2 1)

:nullary        1 2 3 4 ( + )nullary                    -> 1 2 3 4 7
:unary          1 2 3 4 ( + )unary                      -> 1 2 3   7
:binary         1 2 3 4 ( + )binary                     -> 1 2     7
:ternary        1 2 3 4 ( + )ternary                    -> 1       7

:ifte           1 2 3 4 (+ + 7 ==)(77)(99)ifte          -> 1 2 3 4 99
:ifte           1 1 2 4 (+ + 7 ==)(77)(99)ifte          -> 1 1 2 4 77

:land           true  (16)land                          -> 16
:land           false (16)land                          -> false
:land           true  (0)land                           -> 0
:lor            true  (16)lor                           -> true
:lor            false (16)lor                           -> 16
:lor            false (0)lor                            -> 0

:all?           ( 2 4 6 8 )( even? )all?                -> true
:all?           ( 2 4 7 8 )( even? )all?                -> false
:all?           ()         ( even? )all?                -> true
:some?          ( 1 3 4 5 )( even? )some?               -> true
:some?          ( 1 3 5 7 )( even? )some?               -> false
:some?          ()         ( even? )some?               -> false
:none?          ( 1 3 4 5 )( even? )none?               -> false
:none?          ( 1 3 5 7 )( even? )none?               -> true
:none?          ()         ( even? )none?               -> true

:takewhile      ()( zap true )takewhile                 -> ()
:takewhile      ()( zap false )takewhile                -> ()
:takewhile      (1 2 3 4)( zap false )takewhile         -> ()
:takewhile      (1 2 3 4)( zap true )takewhile          -> (1 2 3 4)
:takewhile      (1 2 3 4)( 3 < )takewhile               -> (1 2 )
:takewhile      100 10 up (1 2 3 4)( dupd cpdown + +   113 < )takewhile uzap nip  -> (1 2 )

:keep           100 4 (+)keep                           -> 104 4
:keep2          100 4 5 (+ *)keep2                      -> 900 4 5
:ci             1 (100 dupd +)ci ci ci                  -> 1 (103 dupd +)
:next           100 (1 +)next next next                 -> 103 (1 +)

:mapwith        ( 1 2 3 4 )()(join)mapwith          -> ( 1 2 3 4 )
:mapwith        ( 1 2 3 4 )( 1000 +)(join)mapwith          -> ( 1001 1002 1003 1004 )
:mapwith        ( 1 2 3 4 )( 1000 +)(dup even? if(join)(dup pair append))mapwith   -> ( 1001 1001 1002 1003 1003 1004 )

;============================================================================
)define ; word-tests
;============================================================================
; :chapter:   ASSERT

`Assert.test-failed {} (
                Ink.dbg
                uzap `some-test-failed up
                lf unlist
                (       unswons uprint ':'cprint cpup
                        10 tab ( xprint_ )each
                )dip
                        35 tab
;;;                     lf
                        "  ->" print spc ( xprint_ )each
                        nil down infra
                        50 tab
;;;                     lf
                        "got: "print reverse  ( xprint_ )each
                Ink.std

)define


`Assert.run.tests {tests -- } (
                Symbol.def
                ( `: == )split-at
                1drop    ; test before first :
                (( `-> == )split-at )map
                (       cpup
                        unlist   ( nild   1drop  infra )dip
                        reverse = if( uzap )( down Assert.test-failed  )
                )each
)define

`Assert.run {} (
                `all-tests-passed up
                (   word-tests   )( Assert.run.tests )each
                down
                `all-tests-passed == if()( lff  )
)define


;============================================================================
; :chapter:   TESTQ
(
__testq {}      (   "__testq" `TEST VM.info   3 0 Cursor.go   ( Assert.run )50 times   )Cursor.intermezzo

__testq++ {} (  VM.conscount
                VM.execcounts + +
                VM.rtcticks
                ((
                        ( Ink.__demo            __testq   0 0 Cursor.go)
                        ( Ink.__demo-2          __testq )
                        ( Displaytest.__demo    __testq   Cursor.hide )
                        ( Random.__demo         __testq )
                        ( Turtle.__demo         __testq )
                        ( Stream.__demo-2       __testq )
                        ( Date.__test           __testq )
                )evals) n times
                VM.rtcticks         swap -                      set( t )
                VM.execcounts + +   swap -   0==* when( zap 1 ) set( c )
                VM.conscount        swap -   0==* when( zap 1 ) set( cns )
                Display.clear Ink.chapter
                11 30 Cursor.go  ( `__testq++ `done             )~
                12 33 Cursor.go  ( t  `seconds                  )~
                13 33 Cursor.go  ( c t 16 * / 1000000 /  `MIPS  )~
                Ink.comment
                (       Time.now
                        "       "
                        t 16 div SPC   c t div 16 * 1000000 div SPC   c cns /
                        "       "
                        n c spc cns   Stacks.current triple   GC.run pair
                )log
              )3 times
        .~ {} expand ( xshow )map flatten print
        .t {}
        .c {}
        .cns {}
        .n {} 1


)Define
;============================================================================
; :chapter:   RELATED.WORDS

(
Related.words {} (
                ( dup 2dup 3dup dupd )
                ( chunks chunks/retain-gaps cleave separate split-at filter )
                ( over leap dup nip zap )
                ( rollup rolldown rotate )
                ( headseq? subseq? tailseq? )
                ( Bool.and Bool.or land lor and or patt?* )
                ( all? some? none? )
                ( next nextd keep keep2 ci )
                ( say log xprint xshow show )
                ( ** **. )
                ( XX xx Stacks.?print ? ?? ??? ???? )
                )


Related.tags {} (
                ( bit           "bit operations" )
                ( bool          "boolean operations" )
                ( dbg           "debug" )
                ( dd            "dot documentation" )
                ( find          "find" )
                ( flt           "62-bit floats" )
                ( fmt           "format a string or number   see: #hex" )
                ( fpu           "x87 FPU Floating Point Unit stack, 80-bit floats, stack depth: 8" )
                ( hex           "format hexadecimal          see: #fmt" )
                ( os            "operating system" )
                ( var           "definitions as variables" )
                )


Related.__info {} "Related
Related.words and Related.tags.
"

)Define
;============================================================================
; (c) sts-q 2021-Apr



