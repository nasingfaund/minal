;============================================================================

; :file:   games.minal                  Games for Minal.

;============================================================================
; :chapter:   2048
;-----------------------------------------------------------------------------
(module Game-2048

interface

score {-- i} 0

board {-- TT}  ((0 0 0 0)) 4 repeat


redraw {score board -- score board}
        "    --- 2048 ---" `WORK VM.info
        Ink.std
        6 12 Cursor.go "2048"print
        6 35 Cursor.go score
        9 0 Cursor.go  board
        values
    .score {score board -- score board} over iprint
    .values {board --}
        Ink.std
        0 0
        | dup 4 ==      | zap 0  ^inc  rec
        | over 4 ==     | 2zap
        others          | 3dup TT.elt (((2* x0 +)(7 * y0 +)bi Cursor.go )keep2 )dip  out inc  rec
    .x0 {} 10
    .y0 {} 14
    .out {i --} 0==* if( zap )( show 3 32 fill-centered print )
    .board {}
        Ink.comment
        (
        "          +------+------+------+------+"
        "          |      |      |      |      |"
        "          +------+------+------+------+"
        "          |      |      |      |      |"
        "          +------+------+------+------+"
        "          |      |      |      |      |"
        "          +------+------+------+------+"
        "          |      |      |      |      |"
        "          +------+------+------+------+"
        ) ( print lf )each


we-can-move? {board -- board b}
        | dup                     flatten minimum 0==   | true
        | dup  dup transpose concat a-couple-somewhere? | true
        others                                          | false
    .a-couple-somewhere? {ll -- b}
        ( couple? )map ()some?
    .couple? {l -- b}
        unswons
        | nild?*                | 2zap  false
        | ^unswons ==*          | 2zap  true
        others                  |       rec


sum-up {score board rs -- score board}
        ^^up  up
        (TT.rotate)cpdown times
        ( shift-left line fill )map
        ( TT.rotate ) 4 down - times
        ^down

    .shift-left {l -- l}
        nild
        | nil?*                 | zap reverse
        | uncons over 0==       | (zap 0 join)dip rec
        others                  | ^swons rec

    .line {score l -- score l}
        | aabb?         | 2 rive                ^sum+ sum+      pair
        | aaxx?         | 2 rive                ^sum+           cons
        | xaax?         | uncons  2 rive        ^sum+   cons    cons
        | xxaa?         | 2 rive                 sum+           join
        others          |
    .aabb? {l -- l b}   dup  unlist       eq  ^eq Bool.and
    .aaxx? {l -- l b}   dup  unlist  2zap eq
    .xaax? {l -- l b}   dup  unlist   zap eq   nip
    .xxaa? {l -- l b}   dup  unlist       eq  2nip
    .eq {i i -- b}
        | 0==*          | 2zap false
        others          | ==
    .sum+ {} sum score

    .fill {l -- l}  dup length 4 swap - (0)swap repeat  swoncat
    .score {i -- i} (^down ^down + up up )keep


insert-something {board -- board}
        | dup  flatten minimum 0== not                  |
        | 16 Random.roll cpup  dupd x/y TT.elt 0==      | down x/y   ^^something TT.elt-set
        others                                          | uzap   rec
    .x/y {i -- line col} 4 divmod
    .something {}  100 Random.ppt if( 4 )( 2 )


play {score board -- score board b}
        insert-something
        | redraw        we-can-move? not        |       true
        | Kbd.key!     `ESC =*                  | zap   false
        |              `THREE =*                | zap   false
        |              'c' =*                   | zap   1 sum-up  insert-something      rec     ; up
        |              't' =*                   | zap   2 sum-up  insert-something      rec     ; left
        |              'e' =*                   | zap   3 sum-up  insert-something      rec     ; down
        |              'n' =*                   | zap   0 sum-up  insert-something      rec     ; right
        others                                  | zap   warn/strange-key                rec
    .warn/strange-key {}
        19 0 Cursor.go Ink.std "    To move press Minal style keys `cten` , exit with <ESC>."print
        3 sleep   Display.clear-line


?won {board -- board}
        19 0 Cursor.go
        | dup flatten 2048 member?      | "           won!" print
        others                          | "           not won..." print


run {--}
        clr Cursor.hide

        score board play   when( ?won )   2zap

        Ink.std Cursor.insert


)Define ; Game-2048
;=============================================================================
; (c) sts-q  2021-Jun

