;============================================================================

; :file:   parse.fasm

;=============================================================================
; :chapter:   PARSE

;-----------------------------------------------------------------------------
; :section:             parse macros

macro cc { movzx c, byte [k] }
macro eof? dest { zero? cl, dest }
macro nextcc {
        inc k
        cc
}

macro inc_current_line { add [Parse.current_line], dword 4 }


;-----------------------------------------------------------------------------
; :section:             parse cons_word_or_number

cons_word_or_number:   ; v = start  k = end+1
                mov si,false                            ; si:negsign := false
                mov di,false                            ; di:anysign := false
                mov a, vd
                mov cl, byte [a]
        .skip_plus:
                notequal? cl, '+', .skip_minus
                inc a
                mov di, true
                jmp .digits?
        .skip_minus:
                notequal? cl, '-', .digits?
                inc a
                mov si, true
                mov di, true
            .digits?:
                mov cl, byte [a]                        ; skip possible sign
                cmp cl, '0'
                jl cons_word                            ; this is not a number -> cons_word
                cmp cl, '9'
                jg cons_word                            ; this is not a number -> cons_word
                inc a                                   ; next
            notequal? a,kd, .digits?

        .cons_number:
                clr a, c
                zero? di, .loop                         ; skip sign
                inc v
            .loop:
                imul rax, 10
                mov cl, byte [v]
                sub cl, '0'
                add rax,rcx
                inc v
            notequal? v,k, .loop
                zero? si, .then
                neg rax
        .then:
                int rax
                jmp cons_it                             ; number found -> cons it at done


cons_word:
                range_to_lst vd,kd
                Symbol.symbol
;;;             mov b,[sym_true]                        ; translate symbol true/false to boolean
;;;             equal? a,b,     .cons_true                              ; [sym_true] == TRUE
;;;             mov b,[sym_false]                                       ; [sym_false] == FALSE
;;;             equal? a,b,     .cons_false
;;;             jmp             cons_it
;;;     .cons_true:
;;;             mov rax,TRUE
;;;             jmp             cons_it
;;;     .cons_false:
;;;             mov rax,FALSE
cons_it:
                mov rdi, rax
                cons rdi,tosd,  errin_parser
                cc                                      ; c was used somewhere above
                ret


;-----------------------------------------------------------------------------
; :section:             parse.scan
Parse.scan:  ; (str0:source -- lst:ts )
        clr tos, sos, c, v
        mov kd, a
        cc

    ;-----------------------------------
    pscan:

      pwhitespace:
        equal? cl, 32, pnextcc
        equal? cl, 09, pnextcc


      pcomment:
        notequal? cl, ';', pnewline
        skip_comment:
                nextcc
                eof? pdone_ok
                equal? cl, 10,  do_newline
                equal? cl, 13,  do_newline
                jmp skip_comment


      pnewline:
        equal? cl, 10, do_newline
        equal? cl, 13, do_newline
        jmp paccent
        do_newline:
                inc_current_line
                nextcc
                eof? pdone_ok
                notequal? cl,13, pscan
      pnextcc:
                nextcc
                jmp pscan


      paccent:
        notequal? cl, '^', pspecial
        mov v,k
        skip_accent_chars:
                nextcc
                equal? cl, '^', skip_accent_chars
        call cons_word
        jmp pscan


      pspecial:                               ; ()|#:,`
        equal? cl, '(', cons_special_char
        equal? cl, ')', cons_special_char
        equal? cl, '|', cons_special_char
        equal? cl, '#', cons_special_char
        equal? cl, ':', cons_special_char
        equal? cl, ',', cons_special_char
        equal? cl, '`', cons_special_char
        jmp pstring


      cons_special_char:
        mov v,k
        nextcc
        call cons_word
        jmp pscan


      pstring:
        equal? cl, '{', cons_string_3
        equal? cl, '"', cons_string_2
        jmp                     pstring1

        cons_string_2:
                mov d, '"'
                jmp             skip_string
        cons_string_3:
                mov d, '}'

        skip_string:
                mov v,k
                inc v
        skip_string_do:
                nextcc
                eof? string_terminator_missing
                notequal? cl, 10, @f
                inc_current_line
            @@:
                notequal? cl, dl, skip_string_do

        pget_string:
                lst tos
                range_to_lst vd,kd
                mov di, a
                unlst tos
                notequal? d, '}', pcons_string
            pprepend_docstring:
                mov d, [sym_docstring]
                lst tos
                consLst rdx,di, errin_parser
                unlst tos
            pcons_string:
                cons rdi,tosd, errin_parser
                nextcc
                jmp             pscan




        pstring1:
                notequal? cl, "'", pword_or_number   ; '
                mov d, "'"
                mov v,k
                inc v
        skip_string1:
                nextcc
                eof?            string_terminator_missing
                notequal? cl,10, @f
                inc_current_line
            @@: notequal? cl,dl,skip_string1
                mov a,kd                                 ; length = 1  --> 'c' --> a character
                sub a,vd
                cmp a,1
                je              pget_char1
        pget_string1:                                   ; length != 1 --> '' | '...' --> a string
                lst tos
                range_to_lst vd,kd
                mov di, a
                unlst tos
                cons rdi,tosd, errin_parser
                nextcc
                jmp             pscan

        pget_char1:
                mov cl, byte [v]
                int c
                cons rcx,tosd, errin_parser
                nextcc
                jmp             pscan


      pword_or_number:
        cmp cl, 32
        jle     peof
        cmp cl, 127
        jg      peof
        mov v,k
        skip_word_characters:
                nextcc
                equal? cl, "'", pcons_word
                equal? cl, '"', pcons_word
                equal? cl, "{", pcons_word
                equal? cl, "}", pcons_word
                equal? cl, "(", pcons_word
                equal? cl, ")", pcons_word
                equal? cl, "|", pcons_word
                equal? cl, "#", pcons_word
                equal? cl, ":", pcons_word
                equal? cl, "`", pcons_word
                equal? cl, ",", pcons_word
                equal? cl, ";", pcons_word
                cmp cl, 32
                jle             pcons_word
                cmp cl, 127
                jg              pcons_word
                jmp skip_word_characters
        pcons_word:
        call cons_word_or_number
        jmp pscan


      peof:
        eof? pdone_ok


      pothers:
        mov [Parse.current_error], dword errmess_strange_char_found
        mov [Parse.strange_chr], c
        jmp pdone_failure


    ;-----------------------------------
    string_terminator_missing:
        mov [Parse.current_error], dword errmess_string_terminator_missing
        mov [Parse.strange_chr], d
    pdone_failure:
        mov a,false
        ret


    pdone_ok:
        lst tos
        call revAF
        mov rax, tos
        ret


;-----------------------------------------------------------------------------
; :section:             parse.parse

Parse.parse:  ; ( ts -- ts )
        mov si, a                               ; si := ts:l
        unlst si
        mov sos,NIL                             ; sos := nesting lists
        clr tos                                 ; tos := nts

        zero? si, Parse.parse.done
    .next_token:
        uncons v,si
        equal? v, [sym_openparen],  .nest
        equal? v, [sym_closeparen], .unnest

      .just_cons:
        List.link tosd,  v,tosd, errin_parser, rsi
    notzero? si, .next_token
        jmp Parse.parse.done

      .nest:
        lst tos
        unlst sos
        List.link sosd,  tos,sosd, errin_parser, rsi
        lst sos
        clr tos                                 ; new nested list
    notzero? si, .next_token
        jmp Parse.parse.done

      .unnest:
        lst tos
        call revAF
        NIL? sos, Parse.parse.open_paren_missing
        unconsLst k,sosd
        unlst k
        List.link tosd,  tos,kd, errin_parser, rsi
    notzero? si, .next_token

Parse.parse.done:
        NOTNIL? sos, Parse.parse.closing_paren_missing
        lst tos
        call revAF
        mov rax, tos
        ret

Parse.parse.open_paren_missing:
        mov [Parse.current_error], dword errmess_open_paren_missing
        mov a, false
        ret

Parse.parse.closing_paren_missing:
        mov [Parse.current_error], dword errmess_closing_paren_missing
        mov a, false
        ret


;-----------------------------------------------------------------------------
; :section:             parse rabbit

Parse.rabbit:   ; ( str0:source lst:name  -- lst|false )
                push tos sos
                mov c,ONE
                mov [Parse.current_source], b
                mov [Parse.current_line],   c
                mov [Parse.current_error],  dword 0
                mov [Parse.strange_chr],    dword 0

                call Parse.scan
                zero? a, .failed
                call Parse.parse
    .success:
;;;             pop sos tos
;;;             ret

    .failed:    pop sos tos
                ret


Parse.print_error:

        error_is errmess_parsing_failed
        ink_error

        print_at 3,41
        spc
        mprint [Parse.current_error]
        spaces 35

        print_at 4,41
        spc
        mprint errmess_source
        sprint [Parse.current_source]
        spaces 30

        print_at 5,41
        spc
        mprint errmess_line
        mov a,[Parse.current_line]
        print rax
        spaces 30

        mov a, [Parse.strange_chr]
        zero? a,                .done
        print_at 6,41
        spc
        mprint errmess_char
        mov a, [Parse.strange_chr]
        iprint rax
        spaces 3
        mov a, [Parse.strange_chr]
        cmp a,32
        jl .chr_done
        cmp a,127
        jg .chr_done
        cprint a
    .chr_done:
        spaces 30

    .done: jmp restart


;=============================================================================
; :chapter:   EVAL

macro  eval  name,source { calls evalF, name,source }   ; name:str0  source:lst
evalF:
                mov k,rbx
                str0_to_lst eax
                mov b,a
                mov rax,k
                call Parse.rabbit
                zero? a, .failed

      .success: unlst eax
                exec eax
                ret

       .failed: call Parse.print_error
                ret


macro evalsrc src { eval src#.name, src#.code }


;============================================================================
; (c) sts-q 2021-Apr
