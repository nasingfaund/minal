;============================================================================

; :file:   atoms_add.fasm

;============================================================================

macro addatom fn, name_, docstring {
        local docstring__, then

        jmp then

    fn#.name:           db name_, 0
    docstring__:        db docstring, 0

        then:   Symtab.add  fn#F, fn#.name, docstring__, fn#F.size
}


; -----------------------------------------------------------------------------

macro add_atoms { call add_atomsF }
add_atomsF:

; -----------------------------------------------------------------------------
; :section:             add predefined

addatom docstringA,     "docstring",    "--"
addatom openparenA,     "(",            "--"
addatom closeparenA,    ")",            "--"

; -----------------------------------------------------------------------------
; :section:             add print

addatom printA,         "print",        "x --  // print char string"
addatom cprintA,        "cprint",       "c --  // print character"
addatom iprintA,        "iprint",       "i --  // print integer"
addatom uprintA,        "uprint",       "i --  // print symbol"
addatom lprintA,        "lprint",       "l --  // print list"

addatom spcA,           "spc",          "--"
addatom tabA,           "tab",          "i --"
addatom lfA,            "lf",           "--"
addatom lffA,           "lff",          "--"
addatom crA,            "cr",           "--"

addatom cursor_goA,     "Cursor.go",    "i i --  // line col"
addatom cursor_posA,    "Cursor.pos",   "-- i i  // line col"

addatom inkA,           "Ink.!",        "i -- // print with ink i"
addatom ink_getA,       "Ink.@",        "-- i // get current ink"

addatom display_indentation_setA, "Display.indentation!", "i --  // set display indentation, effecting cr and lf"
addatom display_indentation_getA, "Display.indentation@", "-- i  // get display indentation"

; -----------------------------------------------------------------------------
; :section:             add read

addatom Kbd.scancodeA,  "Kbd.scancode", "-- i  // 0|scancode"


; -----------------------------------------------------------------------------
; :section:             add stack shuffling words

addatom idA,            "id",           "--"
addatom dupA,           "dup",          "x -- x x"
addatom dupdA,          "dupd",         "x y -- x x y"
addatom dup2A,          "2dup",         "x y -- x y x y"
addatom dup3A,          "3dup",         "x y z -- x y z x y z"
addatom zapA,           "zap",          "x -- "
addatom zap2A,          "2zap",         "x y -- "
addatom zap3A,          "3zap",         "x y z -- "
addatom nipA,           "nip",          "x y --  y"
addatom nip2A,          "2nip",         "x y z -- z"
addatom swapA,          "swap",         "x y -- x y"
addatom swapdA,         "swapd",        "x y z -- y x z"

addatom overA,          "over",         "x y -- x y x"
addatom leapA,          "leap",         "x y z -- x y z x"

addatom stackA,         "stack",        "... x y z -- ... x y z (z y x ...)  // push stack as list."
addatom unstackA,       "unstack",      "... (z x y ...) -- ... y x z  // replace stack with elements of l"

; -----------------------------------------------------------------------------
; :section:             add upstack

addatom nilupA,         "nilup",        "  --    //  [  -- nil]  push nil to upstack"
addatom upA,            "up",           "x --    //  [  -- x]    move x up to upstack"
addatom downA,          "down",         "  -- x  //  [x --  ]    move x down from upstack"
addatom cpupA,          "cpup",         "x -- x  //  [  -- x]    copy x to upstack"
addatom cpdownA,        "cpdown",       "  -- x  //  [x -- x]    copy x from upstack"
addatom uzapA,          "uzap",         "  --    //  [x --  ]    zap at upstack"
addatom consupA,        "consup",       "x --    //  [l -- l]    cons x to list at upstack"
addatom unconsdownA,    "unconsdown",   "  -- x  //  [l -- l]    uncons x from list at upstack"
addatom uincA,          "uinc",         "  --    //  [i -- i]    inc i at upstack"
addatom uswapA,         "uswap",        "  --    //  [a b -- b a] swap at upstack"


; -----------------------------------------------------------------------------
; :section:             add integer

addatom addA,           "+",            "i i -- i"
addatom subA,           "-",            "i i -- i"
addatom mulA,           "*",            "i i -- i"
addatom divA,           "div",          "i i -- i"
addatom modA,           "mod",          "i i -- i"
addatom ndivA,          "/",            "i i -- i"

addatom divmodA,        "divmod",       "i i -- i i"
addatom muldivA,        "*/",           "i i i -- i i  // 124-bit intermediate result"

addatom andA,           "and",          "i i -- i  // bitwise and  #bit"
addatom orA,            "or",           "i i -- i  // bitwise or  #bit"
addatom xorA,           "xor",          "i i -- i  // bitwise xor  #bit"

addatom shlA,           "shl",          "i i -- i  // shift left  #bit"
addatom shrA,           "shr",          "i i -- i  // shift right  #bit"
addatom asrA,           "asr",          "i i -- i  // arithmetic shift right  #bit"

addatom absA,           "abs",          "i -- i  // absolute i"
addatom sgnA,           "sgn",          "i -- i  // sign of i"
addatom negA,           "neg",          "i -- i  // negate i: i := -i"
addatom invA,           "inv",          "i -- i  // invert all bits of i: xor i -1  #bit"


addatom incA,           "inc",          "i -- i"
addatom decA,           "dec",          "i -- i"
addatom shr1A,          "2/",           "i -- i  #bit"
addatom shl1A,          "2*",           "i -- i  #bit"

addatom popcntA,        "popcnt",       "i -- i  // number of bits set to 1 in i  #bit"
addatom bsrA,           "bsr",          "i -- i  // bit scan reverse  #bit"
addatom bsfA,           "bsf",          "i -- i  // bit scan forward  #bit"


; -----------------------------------------------------------------------------
; :section:             add floats

addatom frndA,          "rnd",          "f -- i  // round to nearest integer  #flt"
addatom fpiA,           "Pi",           "-- f  #flt"
addatom fpowerA,        "**.",          "x y -- x^y //  #flt"
addatom frecipA,        "1/x",          "x -- f //  #flt"
addatom sqrtA,          "sqrt",         "i -- i  // #flt"


; -----------------------------------------------------------------------------
; :section:             add FPU floats

addatom fclrA,          "fclr",         "--  // clear FPU stack.  #fpu"
addatom fcurrentA,      "fcurrent",     "-- i //  #fpu"
addatom fstoreA,        "f!",        "i --  // store top of FPU stack at address i, 80-bit   #fpu"
addatom ffetchA,        "f@",        "i --  // fetch float from address i to FPU stack, 80-bit  #fpu"
addatom fdumpA,         "fdump",        "-- l  //  #fpu"

addatom fupA,           "fup",          "x --  //  #fpu"
addatom fdownA,         "fdown",        "-- x  //  #fpu"
addatom fzapA,          "fzap",         "--  //  #fpu"
addatom fdupA,          "fdup",         "--  //  #fpu"
addatom fswapA,         "fswap",        "--  //  #fpu"
addatom fswapdA,        "fswapd",       "--  //  #fpu"
addatom foverA,         "fover",        "--  //  #fpu"
addatom fleapA,         "fleap",        "--  //  #fpu"

addatom faddA,          "f+",           "--  // #fpu"
addatom fsubA,          "f-",           "--  // #fpu"
addatom fmulA,          "f*",           "--  // #fpu"
addatom fdivA,          "f/",           "--  // #fpu"
addatom fpu_piA,        "fpi",          "--  // #fpu"
addatom fpu_powerA,     "f**",          "--  //  #flt"
addatom fsqrtA,         "fsqrt",        "--  // #fpu"

addatom f2xm1A,         "f2^x-1",       "--  // (2^x) - 1;   -1 <= x <= 1  // #fpu"
addatom fyl2xA,         "fylog2x",      "--  // y * log2(x)  #fpu"
addatom fyl2xp1A,       "fylog2[x+1]",  "--  // y * log2(x+1)  #fpu"


; -----------------------------------------------------------------------------
; :section:             add boolean

; boolean is a subclass of symbol with two elements: the symbols 'true' and 'false'

addatom trueA,          "true",         "-- b  // true  == symbol true  (parse.fasm compiles symbol true)  #bool"
addatom falseA,         "false",        "-- b  // false == symbol false (parse.fasm compiles symbol false)  #bool"

addatom notA,           "not",          "b -- b  #bool"

addatom boolandA,       "Bool.and",     "b b -- b  #bool"
addatom boolorA,        "Bool.or",      "b b -- b  #bool"

;;;addatom a.if_or,        "?or",          "b --  // if true return from instruction stream"
;;;addatom a.if_and,       "?and",         "b --  // if false return from instruction stream"
;;;addatom a.if_or_cs,     "?or*",         "b -- b  // if true return from instruction stream with true"
;;;addatom a.if_and_cs,    "?and*",        "b -- b  // if false return from instruction stream with false"
;;;
;;;addatom a.land,         "land",         "b q -- b  // if b discard b and exec q, else pass on b:=false"
;;;addatom a.lor,          "lor",          "b q -- b  // if not b discard b and exec q, else pass on b:=true"


; -----------------------------------------------------------------------------
; :section:             add comparision

addatom comp_eqA,       "==",           "x y -- b"
addatom comp_eq_csA,    "==*",          "x y -- x b"
addatom comp_0eqA,      "0==",          "i -- b"
addatom comp_0eq_csA,   "0==*",         "i -- i b"


addatom cmpA,           "cmp",          "x y -- i"


;;;addatom comp_gA,        ">",            "x y -- b"
;;;addatom comp_geA,       ">=",           "x y -- b"

addatom comp_eA,        "=",            "x y -- b"
addatom comp_neA,       "!=",           "x y -- b"

;;;addatom comp_leA,       "<=",           "x y -- b"
;;;addatom comp_lA,        "<",            "x y -- b"


; -----------------------------------------------------------------------------
; :section:             add type tests

addatom int?A,          "int?",         "x -- b  // is x an integer?"
addatom sym?A,          "sym?",         "x -- b  // is x a symbol?"
addatom lst?A,          "lst?",         "x -- b  // is x a list?"
addatom flt?A,          "flt?",         "x -- b  // is x a float?"
addatom int_cs?A,       "int?*",        "x -- x b  // is x an integer?"
addatom sym_cs?A,       "sym?*",        "x -- x b  // is x a symbol?"
addatom lst_cs?A,       "lst?*",        "x -- x b  // is x a list?"
addatom flt_cs?A,       "flt?*",        "x -- x b  // is x a float?"
addatom atom?A,         "atom?",        "x -- b"
addatom bool?A,         "bool?",        "x -- b  #bool"

addatom type?A,         "type?",        "x u -- b  // is x of type u? (a 1 2 3)`a type? -> true, {}`docstring type? -> true"
addatom patt_csA,       "patt?*",       "l l -- l b  // (1 2 3) ((int?) (int?))patt?* -> (1 2 3) true"

; -----------------------------------------------------------------------------
; :section:             add list

addatom consA,         "cons",         "x l -- l"
addatom swonsA,        "swons",        "l x -- l"
addatom swonsdA,       "swonsd",       "l x y -- l y"
addatom unswonsA,      "unswons",      "l -- l x"
addatom concA,         "conc",         "l x -- l  // mutate last element of l to append x, return new last element"
addatom joinA,         "join",         "l x -- l"
addatom unconsA,       "uncons",       "l -- x l"
addatom shuntA,        "shunt",        "l l -- l l  // uncons swonsd"

addatom copyA,         "copy",         "l -- l  // return a copy of l"
addatom reverseA,      "reverse",      "l -- l"
addatom revA,          "rev",          "l -- l  // reverse l in place"
addatom concatA,       "concat",       "l l -- l"
addatom swoncatA,      "swoncat",      "l l -- l  // swap concat"
addatom appendA,       "append",       "l l -- l  // mutate last element of l1 to append l2"
addatom lengthA,       "length",       "l -- i"
addatom is_memberA,    "member?",      "l x -- b  // is x member of l, test with == for int and sym, cmp for lists"
addatom is_equalA,     "equal?",       "l l -- b  // compare with ==, not recursive"

addatom nilA,          "nil",          "-- l  // empty list"
addatom nildA,         "nild",         "x -- l x  // put empty list under x"
addatom is_nilA,       "nil?",         "l -- b  // is l empty?"
addatom is_nilcsA,     "nil?*",        "l -- l b"
addatom is_nildcsA,    "nild?*",       "l x -- l x b"

addatom firstA,        "first",        "l -- x"
addatom first_csA,     "first*",       "l -- l x"
addatom secondA,       "second",       "l -- x"
addatom thirdA,        "third",        "l -- x"
addatom eltA,          "elt",          "l i -- x  // i-th element of l, first has index 0"
addatom drop1A,        "1drop",        "l -- l  // error on empty list"
addatom takeA,         "take",         "l i -- l"
addatom dropA,         "drop",         "l i -- l"

addatom listA,         "list",         "x -- l  // x -> ( x )"
addatom pairA,         "pair",         "x y -- l"
addatom tripleA,       "triple",       "x y z -- l"
addatom unpairA,       "unpair",       "l -- x y"
addatom untripleA,     "untriple",     "l -- x y z"
addatom issolocsA,     "solo?*",       "l -- l b"

addatom unlistA,       "unlist",       "l -- ...  // uncons all elements of l to stack"


addatom eachA,         "each",         "l p --  // apply p to each element of l put on stack  #map"
addatom mapA,          "map",          "l p --    #map"
addatom timesA,        "times",        "p i --  // apply p i times on stack"

        ;;;addatom a.unpair,       "unpair",       "l -- x y"
        ;;;addatom a.untriple,     "untriple",     "l -- x y z"
        ;;;addatom a.separate,     "separate",     "l p -- l l  // apply predicate p to each elt, true -> l1  false -> l2"
        ;;;addatom a.filter,       "filter",       "l p -- l  // apply predicate p to each elt, true -> l1"
        ;;;addatom a.sum,          "sum",          "li -- i  // sum of list of integers, no overflow check (as always)"
        ;;;addatom a.is_member_eq, "member==?",    "l x -- b  // is x member of l, also test lists with =="


; -----------------------------------------------------------------------------
; :section:             add invoke quotation

addatom quoteA,         "`",            " -- u  // put    next item from running quotation on stack."

addatom iA,             "i",            "q -- // apply quotation"
addatom dipA,           "dip",          "x p -- x  // exec p with x removed from stack, put x back afterwards"
addatom dip2A,          "dip2",         "x y p -- x y"

addatom callA,          "call",         "x --  // call integer and symbol like ( x )i, list like  x i"
addatom accent_1A,      "^",            "x -- x  // hiccup next word from instruction stream and dip it"
addatom accent_2A,      "^^",           "x y -- x y  // hiccup word and dip2 it"
addatom accent_3A,      "^^^",          "x y z -- x y z"

addatom jA,             "j",            " --  // hiccup quotation from instruction stream and apply"
addatom hiccupA,        "hiccup",       " -- x  // hiccup next item from calling quotation on stack"
addatom breakA,         "break",        "// return immediately from running quotation"

addatom infraA,         "infra",        "l q -- l  // exec q on l, that is with l as stack"
addatom unaryA,         "unary",        "q -- x"

addatom evalA,          "eval",         "s --  // evaluate string"

; -----------------------------------------------------------------------------
; :section:             add branching

addatom ifA,            "if",           "x q q -- "
addatom if_rlA,         "if/rl",        "x q q -- // if/relinked"


; -----------------------------------------------------------------------------
; :section:             add Definitions

addatom defineA,        "define",       " u s q -- "
addatom applyA,         "apply",        " q u --  // Apply q to definition of u."

addatom nodefA,         "nodef",        "--"
addatom nopA,           "nop",          " --  // no operation"


; -----------------------------------------------------------------------------
; :section:             add Symbol

addatom Symbol.listA,   "Symbol.list",  "-- l  // list of all symbols"
addatom Symbol.docstringA,"Symbol.docstring", "u -- s"
addatom Symbol.nameA,   "Symbol.name",  "u -- s"
addatom Symbol.sourceA, "Symbol.source","u -- s i  // filename and line where symbol was first seen"
addatom Symbol.sizeA,   "Symbol.size",  "u -- i  // size of atom in bytes"
addatom Symbol.countA , "Symbol.count", "u -- i  // how many times was symbol called?"

addatom Symbol.symA,    "Symbol.sym",   "s -- u"
addatom Symbol.defA,    "Symbol.def",   "u -- l"
addatom Symbol.dottA,   "Symbol.dott",  "u u -- u"

; -----------------------------------------------------------------------------
; :section:             add VM

addatom XXA,            "XX",           " --  // print stacks and halt  #dbg"
addatom msleepA,        "msleep",       "i -- // sleep i microseconds, smallest interval: 62.5 msec (1/16 sec)"

addatom OS.haltA,       "OS.halt",      " --  // halt Minal OS"
addatom OS.restartA,    "OS.restart",   " --  // restart Minal OS"
addatom OS.rebootA,     "OS.reboot",    " --  // reboot Minal OS"

addatom VM.versionA,    "VM.version",   " -- s  // version info"
addatom VM.lstfreeA,    "VM.lstfree",   " -- i i // number of free/max list elements"
addatom VM.runtimeA,    "VM.runtime",   " -- i i // vm runtime: sys_clock_gettime/thread-cpu [sec][nanosec]"
addatom VM.uptimeA,     "VM.uptime",    " -- i  // [sec]"
addatom VM.symtabA,     "VM.symtab",    " -- i i  // number of slots in symbol table: size current"
addatom VM.rtcticksA,   "VM.rtcticks",  "-- i  // rtc ticks ( 16 Hz )"
addatom VM.pitticksA,   "VM.pitticks",  "-- i  // pit ticks ( 18.2 Hz )"
addatom VM.rawsizesA,   "VM.rawsizes",  "-- li  // return sizes of certain parts of the assembly source"
addatom VM.execcountsA, "VM.execcounts","-- i i i  // exec_atoms  exec_def  exec_cons"
addatom VM.conscountA,  "VM.conscount", "-- i  // count of conses"

addatom VM.workaA,      "VM.worka",      "--"

addatom GC.runA,        "GC.run",       "-- i i  // run GC, return number of gc runs and free list elements"
addatom GC.now_onA,     "GC.now-on",    "--   // set vm internal GC_NOW_FLAG on"
addatom GC.now_offA,    "GC.now-off",   "--   // set vm internal GC_NOW_FLAG off"
addatom GC.nowA,        "GC.now",       "--   // run GC now if GC_NOW_FLAG is on"


; -----------------------------------------------------------------------------
; :section:             add Stacks

addatom Stacks.clearA,  "Stacks.clear", "--  // clear stack and upstack, not return-stack"
addatom Stacks.currentA,"Stacks.current"," -- i i i  // current usage of ds ss rs"
addatom Stacks.maxA,    "Stacks.max",   " -- i i i  // maximal load of ds ss rs"
addatom Stacks.resetA,  "Stacks.reset", "\q  --//clear stacks, reset return stack, abort program, continue with q"
addatom Stacks.sizeA,   "Stacks.size",  " -- i i i  // size of ds ss rs"
addatom Stacks.wipeA,   "Stacks.wipe",  " --  // zero out all unused stack elements"

; -----------------------------------------------------------------------------
; :section:             add Diversa

addatom Parse.rabbitA,  "Parse.rabbit", "s -- l  // parse sourcebuffer.  s is name of source for errmess."
addatom loadA,          "load",         "s --  // read file into sourcebuffer and execute."


addatom bios_outA,      "BIOS.out",     "i --"
addatom bios_inA,       "BIOS.in",      "-- i"

; -----------------------------------------------------------------------------
; :section:             add outer world modules

;;;addatom a.Clock.msleep, "Clock.msleep", "i --  // sleep i milliseconds"
;;;
;;;addatom a.File.fetch,   "File.fetch",   "s --  // read file s into sourcebuffer"
;;;addatom a.File.store,   "File.store",   "s -- 0|errno // write file s from sourcebuffer"
;;;addatom a.File.is,      "File.exists?", "s -- b  // file exist?"
;;;addatom a.File.unlink,  "File.unlink",  "s -- i  // remove file, return syscall exit code"
;;;addatom a.File.chmod,   "File.chmod",   "s i -- i  // file-name mode: chmod of file, return syscall exit code"
;;;addatom a.File.open,    "File.open",    "s i -- i  // open file s with flags i, mode o644, return fd|errno"
;;;addatom a.File.close,   "File.close",   "i -- i  // close file fd i, return 0|errno"
;;;
;;;addatom a.Stdout.nb,    "Stdout.nb",    "b -- // print newline as 0a0d, not 0a"
;;;addatom a.Stdout.set,   "Stdout.set",   "i -- // set stdout to fd i"
;;;
;;;addatom a.srcbu_print,  "Sourcebuffer.print",   " --  // print current sourcebuffer"
;;;addatom a.srcbu_append, "Sourcebuffer.append",  "s --  // CURRENTLY IT IS FILL, NOT APPEND"
;;;addatom a.srcbu_content,"Sourcebuffer.content", " -- s"
;;;
;;;addatom a.Time.ue,      "Time.ue",      " -- i  // seconds since Unix epoch: 1970/Jan/01 00:00 UTC"
;;;
;;;
;;;addatom a.Xterm.size,   "Xterm.size",   "-- i i  // ws_row ws_col"
;;;
;;;
;;;addatom a.Sys.fork,     "Sys.fork",     "-- i  // pid"
;;;addatom a.Sys.execve,   "Sys.execve",   "s sl sl --  // command args env"
;;;addatom a.Sys.get_pid,  "Sys.get-pid",  "-- i // get process identification (PID)"
;;;addatom a.Sys.pipe2,    "Sys.pipe2",    "i -- i i i // flags -> fd-read fd-write errno"
;;;addatom a.Sys.dup2,     "Sys.dup2",     "i i -- i -- old-fd new-fd  ->  fd|errno"
;;;
;;;addatom a.Sys.fd_close, "Sys.fd-close",  "i --  // close fd"
;;;addatom a.Sys.fd_read,  "Sys.fd-read",   "i --  // read from fd into Sourcebuffer"
;;;addatom a.Sys.fd_write, "Sys.fd-write",  "i --  i // write Sourcebuffer to fd, return number-of-bytes|errno"
;;;addatom a.Sys.fcntl,    "Sys.fcntl",     "i i i -- i  // fd cmd arg  -> i"
;;;
;;;addatom a.Sys.socket,   "Sys.socket",   "i i i -- i // domain type protocol  ->  socket-fd"
;;;addatom a.Sys.socket_init,"Sys.socket-init", "i --  // init socket for http-request at port i"
;;;addatom a.Sys.bind,     "Sys.bind",     "i -- i  // fd -> errno|0"
;;;addatom a.Sys.connect,  "Sys.connect",  "i -- i  // 0|errno"
;;;addatom a.Sys.listen,   "Sys.listen",   "i i -- i  // fd backlog -> 0|errno"
;;;addatom a.Sys.accept,   "Sys.accept",   "i -- i  // listen-fd --> accepted-socket-fd"
;;;
;;;addatom a.Sys.waitid,   "Sys.waitid",   "i -- i  // child-pid -> child-exit-code, wait until childs exits"

;============================================================================

                ret

;============================================================================

