;============================================================================
;;; References:
;;;     - https://github.com/narke/OSDevAsm   (Konstantin Tcholokachvili)
;;;     - http://wiki.osdev.org
;;;     - https://davidad.github.io/blog/2014/02/18/kernel-from-scratch/ (David A. Dalrymple)
;;;     Intel® 64 and IA-32 Architectures
;;;     Software Developer’s Manual
;;;     Volume 3A:
;;;     System Programming Guide, Part 1
;;;     Chapter 3: Protecting Mode Memory Management
;;;     Chapter 4: Paging

;============================================================================
format ELF64
public multiboot_entry as 'multiboot_entry'
use32


;============================================================================
section '.multiboot' writeable

; The multiboot header must come first.
; Setting up the Multiboot header - see GRUB docs for details
; Multiboot header must be aligned on a 8-byte boundary

MBALIGN         =     1 shl 0                  ; Align loaded modules on page boundaries
MEMINFO         =     1 shl 1                  ; Provide memory map
;;; GRAPHICS    =     1 shl 2
GRAPHICS        =     1 shl 0                  ; std 80x25
MAGIC           =     1BADB002h                ; 'magic number' lets bootloader find the header
FLAGS           =     MBALIGN or MEMINFO or GRAPHICS    ; This is the Multiboot 'flag' field
CHECKSUM        =     -(MAGIC + FLAGS)         ; Checksum required to prove that we are multiboot
VBE_MODE        =     0
VBE_WIDTH       =     1024
VBE_HEIGHT      =     768
VBE_DEPTH       =     32

align 4                                        ; align 4 seems to work so far
multiboot_header:
               dd MAGIC
               dd FLAGS
               dd CHECKSUM
               dd 0
               dd 0
               dd 0
               dd 0
               dd 0
               dd VBE_MODE
               dd VBE_WIDTH
               dd VBE_HEIGHT
               dd VBE_DEPTH


;============================================================================
section '.text' executable align 64            ; The beginning of our kernel code

multiboot_entry:                               ; global multiboot_entry


               mov esp, stack_ + (RSTACK_SIZE * ws)   ; set up the stack
               mov [magic], ebx                ; multiboot magic number
               mov [multiboot_info], eax       ; multiboot data structure

        ;------------
        ; Now we're going to set up the page tables for 64-bit mode.
        ; Since this is a minimal example, we're just going to set up a single page.
        ; The 64-bit page table uses four levels of paging,
        ;    PML4E table => PDPTE table => PDE table => PTE table => physical addr
        ; You don't have to use all of them, but you have to use at least the first
        ; three. So we're going to set up PML4E, PDPTE, and PDE tables here, each
        ; with a single entry.

               PML4E_ADDR     = 0x8000          ; page map level 4                      space for 512x8 bytes
               PDPTE_ADDR     = 0x9000          ; page directory pointer table
               PDE_ADDR       = 0xA000          ; page directory table


        ;------------ PML4E, one entry Page-Map-Level-4-Entry
        ; Set up PML4 entry, which will point to PDPT entry.
        ; The low 12 bits of the PML4E entry are zeroed out when it's dereferenced,
        ; and used to encode metadata instead. Here we're setting the Present and
        ; Read/Write bits. You might also want to set the User bit, if you want a
        ; page to remain accessible in user-mode code.
        ; Although we're in 32-bit mode, the table entry is 64 bits. We can just zero
        ; out the upper bits in this case.
               mov eax, PDPTE_ADDR
               or  eax, 011b                    ; Would be 0b111 to set User bit also
               mov [PML4E_ADDR    ], eax
               mov [PML4E_ADDR + 4], dword 0


        ;----------- PDPTE   Page-Directory-Pointer-Table-Entry
        ; Set up 1 PDPTE, which will point to PDE
               mov eax, PDE_ADDR
               or  eax, 011b
               mov [PDPTE_ADDR    ], eax
               mov [PDPTE_ADDR + 4], dword 0


        ;----------- PDE, Page-Directory-Entry
        ; Set up PDE, which will point to the first 2MB page.
        ; We need to set three bits this time:   Present   Read/Write   Page-Size
        ; to indicate that this is the last level of paging in use.
        ;   (0) := 1
        ;   (1) := 1
        ; PS(7) := 1                            ; page-size  == 1 --> entry maps a 2 MB-page
                mov eax, 10000011b
                mov esi, PDE_ADDR
                mov ecx,  32                    ;      32 pages, 2 MB each --> VBox   64 MB   fasm: -m 131072
;;;             mov ecx, 128                    ;     128 pages, 2 MB each --> VBox  256 MB   fasm: -m 524288
;;;             mov ecx, 512                    ; max 512 pages, 2 MB each --> VBox 1024 MB   fasm: -m 924288
           .write_page_entry:
                mov [esi],     eax
                mov [esi + 4], dword 0           ; upper 32 bit of page address are 0
                add esi, 8                       ; next entry
                add eax, 0x200000                ; add 2 MB
           loopnz .write_page_entry


        ;----------- CR3: paging data struct load
        ; Set master (PML4) page table in CR3.
               mov eax, PML4E_ADDR
               mov cr3, eax

        ;----------- CR4: paging
        ; Enable PGE and PAE bits of CR4 to get 64-bit paging available.
        ; CR4.PAE(5) := 1                       ; page address extension
        ; CR4.PGE(7) := 1                       ; page global enable
        ; CR4.PCIDE() := ???                    ; page context ID enable
               mov eax, 10100000b
               mov cr4, eax

        ;----------- MSR: paging mode
        ; Set IA-32e Paging Mode Enable (read: 64-bit mode enable) in the "model-specific
        ; register" (MSR) called Extended Features Enable (EFER).
        ; IA32_EFER.LME( 8) :=  1
        ; IA32_EFER.NXE(11) :=  ???             ; execute-disable enable
               mov ecx, 0xc0000080
               rdmsr                            ; takes ecx as argument, deposits contents of MSR into eax
               or eax, 100000000b               ; bit-8 := 1
               wrmsr                            ; exactly the reverse of rdmsr

        ;----------- CR0: paging on
        ; Enable PG flag of CR0 to actually turn on paging.
        ; CR0.PG(31) := 1
        ; CR0.WP(16) := ???                     ; superuser-mode write access
               mov eax, cr0
               or eax, 0x80000000
               mov cr0, eax


        ;-----------
        ; Load Global Descriptor Table (outdated access control, but needs to be set)
               lgdt [gdt_hdr]

;;;     ;-----------
;;;     ; Load Interrupt Descriptor Table
;;;            lidt [idt_desc]


;;;     ;-----------
;;;     ; Load TSS segment selector
;;;             mov ax, 16
;;;             ltr ax

                mov ax, 0x10
                mov ds, ax
                mov es, ax
                mov fs, ax
                mov gs, ax
                mov ss, ax

               jmp 0x08:_64_bits                ; direct far jump into 64-bit zone.


;============================================================================
use64
_64_bits:

               jmp main


;============================================================================
; 2021-04-09


