;=============================================================================

; :file:   gc.fasm                      Minal Garbage Collector

;=============================================================================
; not gc root:
;       si di   k v c   a b d   ; tag as lst and push to rs if points to useful list
;
; gc root:              collect only    could also be
;       tos sos         lst             int sym
;       sstos           lst             int sym
;       ds ss           lst             int sym
;       rs              lst             anyting
;       prog            l
;       Symtab:
;               fn      l               atom-addr
;               name    lst
;          docstring    lst
;               src     lst
;
; Rabbit-vm currently shows three data types to the user: lst int sym.
; Intern there are 7 data types: lst/int/sym tagged or untagged plus addresses:
;       lst l int i sym s addr
;
; Values tagged as lst get collected.
;
; Possible checks for integrity:
;       lst
;               points into lst-space?          list-space:  list_mem <= X < list_free
;               is 16-byte aligned?
;
;       sym
;               points into sym-space?          symbol-space:   first_atom_address <= X < list_free
;               is 8-byte aligned?
;
;       int
;               hmm?

;-----------------------------------------------------------------------------
; :section:   lib gc

macro GC_NOW_FLAG_OFF { mov [GC_NOW_FLAG], dword 0 }
macro GC_NOW_FLAG_ON  { inc dword [GC_NOW_FLAG] }

macro GC_NOW {
            local .done
                equal? [GC_NOW_FLAG], dword 0, .done
                clr listff
    .done:
}

macro GC_NOW_PINGCHR  c  {
                mov [GC_NOW_PINGCHR_BUFFER], dword c
}
macro GC_NOW_PINGCHR_OUT {
                cprint [GC_NOW_PINGCHR_BUFFER]
}

macro GC_NOW_PINGCHR_CLEAR {                    ; runs in List.link which needs a  -> take b here
                mov b, '.'
                mov [GC_NOW_PINGCHR_BUFFER], b
}


macro gc_done_info {  ; ( runs free -- )
                push rax rbx                            ; push parameter: runs&free
                tab 60
                cprint '<'
                mov a, maximal_list_elements            ; max
                iprint rax
                spc
                mov a, [rsp + ws]                       ; runs
                iprint rax
                spc
                mov a, maximal_list_elements            ; used := (max - free)
                sub a, [rsp]
                iprint rax
                spc
                GC_NOW_PINGCHR_OUT
                cprint '>'
                spc
                spc
                cr
                pop rbx rax
}


;-----------------------------------------------------------------------------
; :section:   mark
;-----------------------------------------------------------------------------
;       if k is a list
;               with each list element
;                       break if element has been marked
;                       set gc-bit of link
;                       recurse on value
;-----------------------------------------------------------------------------
macro mark item {  ;  ( item -- )
            local .mark, .done
                mov rcx, item
                nolst? rcx,       .done                   ; no lst  ->  no gc
                cmp c,list_mem                          ; c is l  ->  rcx is overkill
                jl .done
                cmp c,list_mem_end
                jg .done
    .mark:
                call f.mark                             ; here is where recur happens
    .done:
}

;-----------------------------------------------------------------------------
f.mark: ; ( c:l -- ) ; l is never nil
                push_sd
                mov si, c                               ; si := l
                unlst si

       .loop:   mov v,  [si]                            ; v  := value
                mov di, [si + ws]                       ; di := next
                bt  di, GC_BIT                          ; bt: bit-test
                jc              .done

                mark v                                  ; recur on value

                mov d, di                               ; write link with gc_bit set, mark nil, too
                bts d, GC_BIT                           ; bit-set
                mov [si + ws], d

                mov si, di
                notzero? si,    .loop

       .done:   pop_ds
                ret

;-----------------------------------------------------------------------------
; :section:   sweep

;-----------------------------------------------------------------------------
;       loop over all list elements, from bottom to top one after one
;               if marked then
;                       unmark
;               else
;                       link to last not marked
;-----------------------------------------------------------------------------

sweep:
                mov si, list_mem                        ; si := start of list-mem-space
                mov vd, list_mem_end                    ; v := list-mem-end
                clr di                                  ; di := new list of free elements
                clr c                                   ; c is count of free list elements
                mov k, WALL                             ; overwrite free list elements with WALL

        doloop
                mov a, [si + ws]                        ; a := link
                bt  a, GC_BIT                           ; bit-test
                jnc             .link                   ; if not marked then link

            .unmark:                                    ; if marked then unmark in order to retain
                btr a, GC_BIT                           ; bit-test&reset
                mov [si + ws], a                        ; link := unmarked( link )
                jmp             .nxt

            .link:                                      ; link to new list of free elements
                mov [si],      k                        ; clear old v with WALL
                mov [si + ws], di
                mov di, si
                inc c
            .nxt:
                add si, 12
        until_equal si,vd

                mov listff, di
                zero? c,        .gc_err_no_space_left
                mov a, c                                ; return length of new free list
                ret


       .gc_err_no_space_left:
                lf
                mprint errmess_gc_out_of_mem
                lf
                jmp exit_fail


;-----------------------------------------------------------------------------
; :section:   mark-stacks
        macro do_mark_stack {
                local .loop, .while
            doloop
                mark [si]
                add si, ws
            until_equal si,di
        }

macro mark_data_stack {
                mov si, dsp
                mov di, border_2
                do_mark_stack
}


macro mark_software_stack {
                mov si, ssp
                mov di, border_3
                do_mark_stack
}


macro mark_return_stack {
                mov rsi, rsp                            ; points full
                add si, (ws * 6)                        ; not regs pushed by gc: si di k v c d
                mov di, stack_end
                do_mark_stack
}


;-----------------------------------------------------------------------------
; :section:   mark-symtab

macro mark_symtab {
        local .loop, .def_done, .while
                mov si, symtab                          ; mark symtab: fn, name, docstring, src
                mov di, [symtab_ff]

            doloop
                mov k, [si]                             ; k := fn
                cmp kd, dsp
                jl              .def_done               ; don't mark atoms
                lst k
                mark k
             .def_done:
                mark [si + o_name]
                mark [si + o_docstring]
                mark [si + o_pSrc]
                add si, tFunc_size
            until_equal si,di
}

;-----------------------------------------------------------------------------
; :section:   gc                        mark and sweep

List.gc:  ;  ( -- runs free )   call with: 'callfun List.gc'    (callee saves)

                cli
                push rsi rdi k v rcx rdx
;;;             app <print_at 4,55>, <info "<gc>">

                mark tos
                mark sos
                mark sstos
                lst prog
                mark progq
                unlst prog

            l1: mark_data_stack
            l2: mark_software_stack
            l3: mark_return_stack
            l4: mark_symtab
                mark [Parse.current_source]

                call sweep                              ; sweep ( -- free )

                mov b, c                                ; return free in b
                mov a, [List.gc.runs]
                inc a
                mov [List.gc.runs], a                   ; return runs in a

                pop rdx rcx v k rdi rsi
;;;             gc_done_info
                sti
                ret



;=============================================================================
; (c) sts-q 2021-Mai

